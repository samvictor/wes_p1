#include "ske.h"
#include "prf.h"
#include <openssl/sha.h>
#include <openssl/evp.h>
#include <openssl/hmac.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* memcpy */
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#ifdef LINUX
#define MMAP_SEQ MAP_PRIVATE|MAP_POPULATE
#else
#define MMAP_SEQ MAP_PRIVATE
#endif

/* NOTE: since we use counter mode, we don't need padding, as the
 * ciphertext length will be the same as that of the plaintext.
 * Here's the message format we'll use for the ciphertext:
 * +------------+--------------------+-------------------------------+
 * | 16 byte IV | C = AES(plaintext) | HMAC(C) (32 bytes for SHA256) |
 * +------------+--------------------+-------------------------------+
 * */

/* we'll use hmac with sha256, which produces 32 byte output */
#define HM_LEN 32
#define KDF_KEY "qVHqkOVJLb7EolR9dsAMVwH1hRCYVx#I"
/* need to make sure KDF is orthogonal to other hash functions, like
 * the one used in the KDF, so we use hmac with a key. */

int ske_keyGen(SKE_KEY* K, unsigned char* entropy, size_t entLen)
{
	/* TODO: write this.  If entropy is given, apply a KDF to it to get
	 * the keys (something like HMAC-SHA512 with KDF_KEY will work).
	 * If entropy is null, just get a random key (you can use the PRF). */
	 
	if (0)
	{
		unsigned char* key;
		key = HMAC(EVP_sha512(), KDF_KEY, strlen(KDF_KEY), entropy, entLen, NULL, NULL);
		memcpy(&(K->hmacKey), key, entLen);
		
		printf("key = %u\n", *(key));
		printf("hmac = %u\n\n", *(K->hmacKey));
		
	}
	else
	{
		//memset(K->hmacKey,0,HM_LEN*16);
		//memset(K->aesKey,0,HM_LEN*16);
		
		unsigned char* key = malloc(HM_LEN*8);
		randBytes(key, HM_LEN);
		memcpy(&(K->hmacKey), key, HM_LEN);
		
		//unsigned char* key2 = malloc(HM_LEN*8);
		randBytes(key, HM_LEN);
		memcpy(&(K->aesKey), key, HM_LEN);
		
		//int i;
		//for(i = 0; i < HM_LEN*3+1; i++)
		//{
		//	printf("aes = %u, ", *(K->aesKey + i));
		//	printf("hmac = %u\n", *(K->hmacKey + i));
		//}
	}
	 
	return 0;
}
size_t ske_getOutputLen(size_t inputLen)
{
	return AES_BLOCK_SIZE + inputLen + HM_LEN;
}
size_t ske_encrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		SKE_KEY* K, unsigned char* IV)
{
	/* TODO: finish writing this.  Look at ctr_example() in aes-example.c
	 * for a hint.  Also, be sure to setup a random IV if none was given.
	 * You can assume outBuf has enough space for the result. */
	
	// Setup aes
	int i;
	if (IV == NULL)
	{
		IV = malloc(HM_LEN/2);
		for (i = 0; i < 16; i++)
			IV[i] = i;
	}
	
	int IV_len = HM_LEN/2;
	
	//for(i = 0; i< HM_LEN; i++)
	//	printf("IV = %u , Temp_IV = %u\n", *(IV+i), *(temp_IV + i));
	//printf("\n");
	
	//printf("in = ");
	//int j;
	//for (j = 0; j < len; j++) {
	//	printf("%c",inBuf[j]);
	//}
	//printf("\n");
	
	// encrypt aes
	unsigned char aes_out[len*2];
	memset(aes_out, 0, len*2);
	
	EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
	if (1!=EVP_EncryptInit_ex(ctx, EVP_aes_256_ctr(), 0, K->aesKey, IV))
		fprintf(stderr, "error with cypher encrypt init\n");
	int nWritten;
	if (1!=EVP_EncryptUpdate(ctx, aes_out, &nWritten, inBuf, len))
		fprintf(stderr, "error with cypher encrypt update\n");
	EVP_CIPHER_CTX_free(ctx);
	
	//printf("inBuf = ");
	//for(i = 0; i < len; i++)
	//	printf("%c", inBuf[i]);
	//printf("\n");
	
	//printf("AES = ");
	size_t aes_len = nWritten;
	//for (i = 0; i < aes_len+2; i++) {
	//	printf("%02x", aes_out[i]);
	//}
	//printf("\n");
	
	//printf("out2 = ");
	//for (i = 0; i < out_len2; i++) {
	//	printf("%02x",out[i]);
	//}
	//printf("\n");
	
	
	//printf("aeslen = %d\n", aes_len);
	//printf("get out = %d\n", ske_getOutputLen(len));
	//printf("block size = %d\n", AES_BLOCK_SIZE);
	//printf("get out - aeslen - block size = %d\n\n", (ske_getOutputLen(len) - aes_len - AES_BLOCK_SIZE));
	
	// setup hmac
	unsigned int * mac_len = malloc(8);
	*mac_len = HM_LEN;
	unsigned char mac_out[*mac_len];
	memset(mac_out, 0, *mac_len);
	unsigned char mac_in[aes_len + IV_len];
	
	memcpy(mac_in, IV, IV_len);
	memcpy(mac_in+IV_len, aes_out, aes_len);
	
	// encrypt hmac
	HMAC(EVP_sha256(), K->hmacKey, HM_LEN, mac_in, aes_len + IV_len, mac_out, mac_len);
	
	//printf("length is %d, and %d\n", *mac_len, len);
	//printf("mac_out = ");
	//int k;
	//for (k = 0; k < *mac_len; k++) {
	//	printf("%02x",mac_out[k]);
	//}
	//printf("\n\n");
	
	// combine
	memset(outBuf, 0, ske_getOutputLen(len));
	memcpy(outBuf, IV, IV_len);
	memcpy(outBuf + IV_len, aes_out, aes_len);
	memcpy(outBuf + IV_len + aes_len, mac_out, *mac_len);
	
	
	//printf("Encrypt:\nIV = ");
	//for(i = 0; i < IV_len; i++)
	//	printf("%02x", IV[i]);
	
	//printf("\nAES = ");
	//for(i = 0; i < aes_len; i++)
	//	printf("%02x", aes_out[i]);
	
	//printf("\nMac = ");
	//for(i = 0; i < HM_LEN; i++)
	//	printf("%02x", mac_out[i]);
	//printf("\n");
	
	//printf("Mac in = ");
	//for(i = 0; i < aes_len + IV_len; i++)
	//	printf("%02x", mac_in[i]);
	//printf("\n");
	
	
	//printf("outBuf = ");
	//for(i = 0; i < HM_LEN/2 + aes_len + *mac_len; i++)
	//	printf("%02x", *(outBuf+i));
	
	//printf("\n\n");
	//printf("%d\n", HM_LEN/2 + aes_len + *mac_len - ske_getOutputLen(len));
	
	
	return IV_len + aes_len + HM_LEN; /* TODO: should return number of bytes written, which
	             hopefully matches ske_getOutputLen(...). */
}
size_t ske_encrypt_file(const char* fnout, const char* fnin,
		SKE_KEY* K, unsigned char* IV, size_t offset_out)
{
	/* TODO: write this.  Hint: mmap. */
	
	int in_i, out_i;
	//printf("fnin = %s\nfnout = %s\n", fnin, fnout);
	
	if ((in_i = open(fnin, O_RDONLY)) < 0)
		printf("error opening input file %s\n", fnin);
	//else
	//	printf("succesfully opened %s\nin_i = %d\n", fnin, in_i);
	
	if ((out_i = open(fnout, O_RDWR | O_CREAT, 0666)) < 0)
		printf("error opening output file %s\n", fnout);
	//else
	//	printf("succesfully opened %s\nout_i = %d\n", fnout, out_i);
	
	
	struct stat in_stats;
	
	if(fstat(in_i, &in_stats) < 0)
		printf("error getting file stats\n");
	//else
	//	printf("got stats: size = %zd\n", in_stats.st_size);
	
	
	unsigned char* infile_data;
	unsigned char* outfile_data;
	infile_data = malloc(in_stats.st_size);
	outfile_data = malloc(in_stats.st_size*2);
	
	size_t in_len;
	if((in_len = read(in_i, infile_data, in_stats.st_size)) < 0)
		printf("error reading data from %s\n", fnin);
	//else
	//	printf("read %s...\nread = %d\n", infile_data, in_len);
	
	size_t out_len = ske_encrypt(outfile_data, infile_data, in_len, K, IV);
	
	//printf("encrypted = ");
	//int i;
	//for(i = 0; i < out_len; i++)
	//	printf("%02x", *(outfile_data+i));
	//printf("\n\n");
	
	
	if(write(out_i, outfile_data, out_len)<0)
		printf("error writing to outfile %s\n", fnout);
	//else
	//	printf("successfully written to %s\n", fnout);
	
	//printf("\n\n\n");
	
	return 0;
}
size_t ske_decrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		SKE_KEY* K)
{
	/* TODO: write this.  Make sure you check the mac before decypting!
	 * Oh, and also, return -1 if the ciphertext is found invalid.
	 * Otherwise, return the number of bytes written.  See aes-example.c
	 * for how to do basic decryption. */
	
	int IV_len = HM_LEN/2;
	int c_len = len - IV_len - HM_LEN;
	unsigned char* cypher = malloc(c_len);
	unsigned char* mac = malloc(HM_LEN);
	unsigned char* IV = malloc(IV_len);
	memcpy(IV, inBuf, IV_len);
	memcpy(cypher, inBuf + IV_len, c_len);
	memcpy(mac, inBuf + IV_len + c_len, HM_LEN);
	
	//int i;
	
	//printf("Decrypt:\nIV = ");
	//for(i = 0; i < IV_len; i++)
	//	printf("%02x", IV[i]);
	
	//printf("\nCypher = ");
	//for(i = 0; i < c_len; i++)
	//	printf("%02x", cypher[i]);
	
	//printf("\nMac = ");
	//for(i = 0; i < HM_LEN; i++)
	//	printf("%02x", mac[i]);
	//printf("\n");
	
	
	// check mac
	unsigned char* mac_out = malloc(HM_LEN);
	unsigned int * mac_len = malloc(8);
	unsigned char mac_in[c_len + IV_len];
	memcpy(mac_in, IV, IV_len);
	memcpy(mac_in + IV_len, cypher, c_len);
	
	//printf("Mac in = ");
	//for(i = 0; i < c_len + IV_len; i++)
	//	printf("%02x", mac_in[i]);
	//printf("\n");
	
	*mac_len = HM_LEN;
	HMAC(EVP_sha256(), K->hmacKey, HM_LEN, mac_in, IV_len + c_len, mac_out, mac_len);
	
	int diff = memcmp(mac, mac_out, HM_LEN);
	
	//printf("New Mac = ");
	//for(i = 0; i < HM_LEN; i++)
	//	printf("%02x", mac_out[i]);
	//printf("\n\n");
	
	if (diff)
	{
		//*outBuf = 193;
		//printf("\nbad %c\n\n", *outBuf);
		*outBuf = 'V';
		return -1;
	}
	
	// decrypt
	unsigned char* out = malloc(len);
	int nWritten = 0;
	
	EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
	if (1!=EVP_DecryptInit_ex(ctx,EVP_aes_256_ctr(),0,K->aesKey,IV))
		fprintf(stderr, "error with cypher decrypt init\n");
	if (1!=EVP_DecryptUpdate(ctx,out,&nWritten,cypher,c_len))
		fprintf(stderr, "error with cypher decrypt update\n");
	
	size_t out_len	= nWritten;
	//printf("out = ");
	//for(i = 0; i<out_len; i++)
	//	printf("%c", *(out+i));
	
	//printf("\n");
	
	memcpy(outBuf, out, out_len);
	
	return out_len;
}
size_t ske_decrypt_file(const char* fnout, const char* fnin,
		SKE_KEY* K, size_t offset_in)
{
	/* TODO: write this. */
	
	
	int in_i, out_i;
	//printf("fnin = %s\nfnout = %s\n", fnin, fnout);
	
	if ((in_i = open(fnin, O_RDONLY)) < 0)
		printf("error openning input file %s\n", fnin);
	//else
	//	printf("succesfully opened %s\nin_i = %d\n", fnin, in_i);
	
	if ((out_i = open(fnout, O_RDWR | O_CREAT, 0666)) < 0)
		printf("error openning output file %s\n", fnout);
	//else
	//	printf("succesfully opened %s\nout_i = %d\n", fnout, out_i);
	
	struct stat in_stats;
	
	if(fstat(in_i, &in_stats) < 0)
		printf("error getting file stats\n");
	//else
	//	printf("got stats: size = %zd\n", in_stats.st_size);
	
	
	unsigned char* infile_data;
	unsigned char* outfile_data;
	infile_data = malloc(in_stats.st_size);
	outfile_data = malloc(in_stats.st_size);
	
	size_t in_len;
	if((in_len = read(in_i, infile_data, in_stats.st_size)) < 0)
		printf("error reading data from %s\n", fnin);
	//else
	//	printf("read %s...\nread = %d\n", infile_data, in_len);
		//printf("read ");
	
	size_t out_len = ske_decrypt(outfile_data, infile_data, in_len, K);
	//printf("out_len = %d\n", out_len);
	//printf("decrypted = ");
	//int i;
	//for(i = 0; i < out_len; i++)
	//	printf("%c", *(outfile_data+i));
	
	//printf("\n\n");
	
	
	if(write(out_i, outfile_data, out_len)<0)
		printf("error writing to outfile %s\n", fnout);
	//else
	//	printf("successfully written to %s\n", fnout);
	
	//printf("\n\n\n");
	
	
	return 0;
}
