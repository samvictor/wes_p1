/* kem-enc.c
 * simple encryption utility providing CCA2 security.
 * based on the KEM/DEM hybrid model. */

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <fcntl.h>
#include <openssl/sha.h>

#include "ske.h"
#include "rsa.h"
#include "prf.h"

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Encrypt or decrypt data.\n\n"
"   -i,--in     FILE   read input from FILE.\n"
"   -o,--out    FILE   write output to FILE.\n"
"   -k,--key    FILE   the key.\n"
"   -r,--rand   FILE   use FILE to seed RNG (defaults to /dev/urandom).\n"
"   -e,--enc           encrypt (this is the default action).\n"
"   -d,--dec           decrypt.\n"
"   -g,--gen    FILE   generate new key and write to FILE{,.pub}\n"
"   -b,--BITS   NBITS  length of new key (NOTE: this corresponds to the\n"
"                      RSA key; the symmetric key will always be 256 bits).\n"
"                      Defaults to %lu.\n"
"   --help             show this message and exit.\n";

#define FNLEN 255

enum modes {
	ENC,
	DEC,
	GEN
};

/* Let SK denote the symmetric key.  Then to format ciphertext, we
 * simply concatenate:
 * +------------+----------------+
 * | RSA-KEM(X) | SKE ciphertext |
 * +------------+----------------+
 * NOTE: reading such a file is only useful if you have the key,
 * and from the key you can infer the length of the RSA ciphertext.
 * We'll construct our KEM as KEM(X) := RSA(X)|H(X), and define the
 * key to be SK = KDF(X).  Naturally H and KDF need to be "orthogonal",
 * so we will use different hash functions:  H := SHA256, while
 * KDF := HMAC-SHA512, where the key to the hmac is defined in ske.c
 * (see KDF_KEY).
 * */

#define HASHLEN 32 /* for sha256 */

int kem_encrypt(const char* fnOut, const char* fnIn, RSA_KEY* K)
{
	/* TODO: encapsulate random symmetric key (SK) using RSA and SHA256;
	 * encrypt fnIn with SK; concatenate encapsulation and cihpertext;
	 * write to fnOut. */
	
	// Get SKE key
	SKE_KEY SK;
	ske_keyGen(&SK,0,0);
	unsigned char IV[16];
	int i;
	for (i = 0; i < 16; i++)
		IV[i] = i;
	
	
	int in_i, out_i;
	//printf("fnIn = %s\nfnOut = %s\n", fnIn, fnOut);
	
	// Setup infile
	if ((in_i = open(fnIn, O_RDONLY)) < 0)
		printf("error opening input file %s\n", fnIn);
	//else
	//	printf("succesfully opened %s\nin_i = %d\n", fnIn, in_i);
	
	if ((out_i = open(fnOut, O_RDWR | O_CREAT, 0666)) < 0)
		printf("error opening output file %s\n", fnOut);
	//else
	//	printf("succesfully opened %s\nout_i = %d\n", fnOut, out_i);
	
	struct stat in_stats;
	
	int temp = fstat(in_i, &in_stats);
	if(temp < 0)
		printf("error getting file stats\n");
	//else
	//	printf("got stats: size = %zd\n", in_stats.st_size);
	
	// Set up symmetric SKE encryption
	unsigned char* infile_data;
	unsigned char* cypher_data;
	infile_data = malloc(in_stats.st_size);
	cypher_data = malloc(in_stats.st_size*8);
	
	// get raw data from infile
	size_t in_len = read(in_i, infile_data, in_stats.st_size);
	
	if(in_len < 0)
		printf("error reading data from %s\n", infile_data);
	//else
	//	printf("read %s...\nread = %d\n", infile_data, in_len);
	
	// encrypt raw data using symmetric SKE encryption
	size_t cypher_len = ske_encrypt(cypher_data, infile_data, in_len, &SK, IV);
	
	//printf("encrypted = ");
	//int i;
	//for(i = 0; i < out_len; i++)
	//	printf("%02x", *(outfile_data+i));
	//printf("\n\n");
	
	// encrypt SKE key using RSA
	// first 32 is SK->hmacKey, second 32 is SK->aesKey
	int rsa_len = 64;
	unsigned char * rsa_in = malloc(rsa_len);
	unsigned char * rsa_out =  malloc(rsa_len*8);
	memcpy(rsa_in, SK.hmacKey, rsa_len/2);
	memcpy(rsa_in + rsa_len/2, SK.aesKey, rsa_len/2);
	
	//printf("SKE hmac Key = ");
	//for(i = 0; i < rsa_len/2; i++)
	//	printf("%02x", *(SK.hmacKey + i));
	//printf("\n");
	//printf("SKE aes Key = ");
	//for(i = 0; i < rsa_len/2; i++)
	//	printf("%02x", *(SK.aesKey + i));
	//printf("\n");
	
	
	//printf("SKE Key = ");
	//for(i = 0; i < rsa_len; i++)
	//	printf("%02x", *(rsa_in + i));
	//printf("\n");
	
	size_t actual_rlen = rsa_encrypt(rsa_out, rsa_in, rsa_len, K);
	
	// concat rsa len | encrypted SKE key | cypher text from SKE encryption
	unsigned char * to_file = malloc(sizeof(size_t) + actual_rlen + cypher_len);
	memcpy(to_file, &actual_rlen, sizeof(size_t));
	memcpy(to_file + sizeof(size_t), rsa_out, actual_rlen);
	memcpy(to_file + sizeof(size_t) + actual_rlen, cypher_data, cypher_len);
	
	
	//printf("actual_rlen = %02x\n", actual_rlen);
	
	//printf("rsa_out = ");
	//for(i = 0; i < actual_rlen; i++)
	//	printf("%02x", *(rsa_out + i));
	//printf("\n");
	
	//printf("cypher_data = ");
	//for(i = 0; i < cypher_len; i++)
	//	printf("%02x", *(cypher_data + i));
	//printf("\n\n");
	
	//printf("to_file = ");
	//for(i = 0; i < sizeof(size_t) + actual_rlen + cypher_len; i++)
	//	printf("%02x", *(to_file + i));
	//printf("\n\n");
	
	// Write to file
	printf("Writing encrypted data to %s\n", fnOut);
	temp = write(out_i, to_file, sizeof(size_t) + actual_rlen + cypher_len);
	if(temp < 0)
		printf("error writing to outfile %s\n", fnOut);
	//else
	//	printf("successfully written to %s\n", fnOut);
	
	//printf("\n\n\n");
	
	return 0;
}

/* NOTE: make sure you check the decapsulation is valid before continuing */
int kem_decrypt(const char* fnOut, const char* fnIn, RSA_KEY* K)
{
	/* TODO: write this. */
	/* step 1: recover the symmetric key */
	/* step 2: check decapsulation */
	/* step 3: derive key from ephemKey and decrypt data. */
	
	int in_i, out_i;
	
	// Setup infile
	if ((in_i = open(fnIn, O_RDONLY)) < 0)
		printf("error opening input file %s\n", fnIn);
	//else
	//	printf("succesfully opened %s\nin_i = %d\n", fnIn, in_i);
	
	
	if ((out_i = open(fnOut, O_RDWR | O_CREAT, 0666)) < 0)
		printf("error opening output file %s\n", fnOut);
	//else
	//	printf("succesfully opened %s\nout_i = %d\n", fnOut, out_i);
	
	struct stat in_stats;
	
	if(fstat(in_i, &in_stats) < 0)
		printf("error getting file stats\n");
	//else
	//	printf("got stats: size = %zd\n", in_stats.st_size);
	
	unsigned char* infile_data;
	infile_data = malloc(in_stats.st_size);
	
	// get raw data from infile
	size_t in_len = read(in_i, infile_data, in_stats.st_size);
	if(in_len < 0)
		printf("error reading data from %s\n", infile_data);
	//else
	//	printf("read %s...\nread = %d\n", infile_data, in_len);
	
	//printf("read = ");
	//int i;
	//for(i = 0; i < in_len; i++)
	//	printf("%02x", *(infile_data+i));
	//printf("\n\n");
	
	// get rsa length
	size_t rsa_len = 0;
	memcpy(&rsa_len, infile_data, sizeof(size_t));
	
	// get rsa data
	unsigned char* rsa_in;
	rsa_in = malloc(rsa_len);
	
	memcpy(rsa_in, infile_data + sizeof(size_t), rsa_len);
	
	// get cypher data
	size_t cypher_len = in_stats.st_size - (sizeof(size_t) + rsa_len);
	unsigned char* cypher_data;
	cypher_data = malloc(cypher_len);
	
	memcpy(cypher_data, infile_data + sizeof(size_t) + rsa_len, cypher_len);
	
	//printf("rsa_len = %02x\n", rsa_len);
	
	//printf("rsa_in = ");
	//for(i = 0; i < rsa_len; i++)
	//	printf("%02x", *(rsa_in + i));
	//printf("\n");
	
	//printf("cypher_data = ");
	//for(i = 0; i < cypher_len; i++)
	//	printf("%02x", *(cypher_data + i));
	//printf("\n");
	
	// getting SKE Key
	int SK_len = 64;
	unsigned char* SKE_Key;
	SKE_Key = malloc(64);
	rsa_decrypt(SKE_Key, rsa_in, rsa_len, K);
	
	//printf("SKE Key = ");
	//for(i = 0; i < SK_len; i++)
	//	printf("%02x", *(SKE_Key + i));
	//printf("\n");
	
	// hmacKey | aesKey
	SKE_KEY SK;
	memcpy(SK.hmacKey, SKE_Key, SK_len/2);
	memcpy(SK.aesKey, SKE_Key + SK_len/2, SK_len/2);
	
	//printf("SKE hmac Key = ");
	//for(i = 0; i < SK_len/2; i++)
	//	printf("%02x", *(SK.hmacKey + i));
	//printf("\n");
	//printf("SKE aes Key = ");
	//for(i = 0; i < SK_len/2; i++)
	//	printf("%02x", *(SK.aesKey + i));
	//printf("\n");
	
	// decrypt cypher text to original message
	// if mac is bad, return -1
	unsigned char * message = malloc(cypher_len);
	size_t m_len = ske_decrypt(message, cypher_data, cypher_len, &SK);
	
	if (m_len < 0)
		return -1;
	
	//printf("message = ");
	//for(i = 0; i < m_len; i++)
	//	printf("%c", *(message + i));
	//printf("\n\n");
	
	
	// Write to file
	printf("Writing decrypted data to %s\n", fnOut);
	if(write(out_i, message, m_len)<0)
		printf("error writing to outfile %s\n", fnOut);
	//else
	//	printf("successfully written to %s\n", fnOut);
	
	
	return 0;
}

int main(int argc, char *argv[]) {
	/* define long options */
	static struct option long_opts[] = {
		{"in",      required_argument, 0, 'i'},
		{"out",     required_argument, 0, 'o'},
		{"key",     required_argument, 0, 'k'},
		{"rand",    required_argument, 0, 'r'},
		{"gen",     required_argument, 0, 'g'},
		{"bits",    required_argument, 0, 'b'},
		{"enc",     no_argument,       0, 'e'},
		{"dec",     no_argument,       0, 'd'},
		{"help",    no_argument,       0, 'h'},
		{0,0,0,0}
	};
	/* process options: */
	char c;
	int opt_index = 0;
	char fnRnd[FNLEN+1] = "/dev/urandom";
	fnRnd[FNLEN] = 0;
	char fnIn[FNLEN+1];
	char fnOut[FNLEN+1];
	char fnKey[FNLEN+1];
	memset(fnIn,0,FNLEN+1);
	memset(fnOut,0,FNLEN+1);
	memset(fnKey,0,FNLEN+1);
	int mode = ENC;
	// size_t nBits = 2048;
	size_t nBits = 1024;
	while ((c = getopt_long(argc, argv, "edhi:o:k:r:g:b:", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'h':
				printf(usage,argv[0],nBits);
				return 0;
			case 'i':
				strncpy(fnIn,optarg,FNLEN);
				break;
			case 'o':
				strncpy(fnOut,optarg,FNLEN);
				break;
			case 'k':
				strncpy(fnKey,optarg,FNLEN);
				break;
			case 'r':
				strncpy(fnRnd,optarg,FNLEN);
				break;
			case 'e':
				mode = ENC;
				break;
			case 'd':
				mode = DEC;
				break;
			case 'g':
				mode = GEN;
				strncpy(fnOut,optarg,FNLEN);
				break;
			case 'b':
				nBits = atol(optarg);
				break;
			case '?':
				printf(usage,argv[0],nBits);
				return 1;
		}
	}

	/* TODO: finish this off.  Be sure to erase sensitive data
	 * like private keys when you're done with them (see the
	 * rsa_shredKey function). */
	printf("\n");
	RSA_KEY K;
	char* pub_str;
	char* pvt_str;
	FILE * pub;
	FILE * pvt;
	
	
	switch (mode) {
		case ENC:
			printf("Encrypting...\n");
			printf("Getting public key from %s\n", fnKey);
			
			pub = fopen(fnKey, "r");
			rsa_readPublic(pub, &K);
			
			//printf("Key read. K->e = %u\n", mpz_get_ui(K.e));
			kem_encrypt(fnOut, fnIn, &K);
			//printf("Shredding local key...");
			rsa_shredKey(&K);
			
			printf("Done!");
			printf("\n");
			break;
			
		case DEC:
			printf("Decrypting...\n");
			
			pvt_str = malloc(strlen(fnKey) + 4);
			strcpy(pvt_str, fnKey);
			strcat(pvt_str, ".pvt");
			printf("Getting private key from %s\n", pvt_str);
			
			pvt = fopen(pvt_str, "r");
			rsa_readPrivate(pvt, &K);
			//printf("Key read. K->d = %u\n", mpz_get_ui(K.d));
			
			kem_decrypt(fnOut, fnIn, &K);
			rsa_shredKey(&K);
			
			printf("Done!");
			printf("\n");
			break;
			
		case GEN:
			printf("Generating %zd-bit key...\n", nBits);
			rsa_keyGen(nBits,&K);
			
			printf("Writing to %s\n", fnOut);
			
			pub_str = malloc(strlen(fnOut) + 4);
			strcpy(pub_str, fnOut);
			strcat(pub_str, ".pub");
			printf("Public key in %s\n", pub_str);
			
			pvt_str = malloc(strlen(fnOut) + 4);
			strcpy(pvt_str, fnOut);
			strcat(pvt_str, ".pvt");
			printf("Private key in %s\n", pvt_str);
			
			
			pub = fopen(pub_str, "w+");
			pvt = fopen(pvt_str, "w+");
			rsa_writePublic(pub, &K);
			rsa_writePrivate(pvt, &K);
			//printf("Key writen. K->e = %u\n", mpz_get_ui(K.e));
			
			//printf("Key saved. Shredding local key...\n");
			rsa_shredKey(&K);
			printf("Done!");
			printf("\n");
			break;

		default:
			return 1;
	}
	

	return 0;
}
