/* test code for RSA */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "../rsa.h"
#include "../prf.h"

/* turn this on to print more stuff. */
#define VDEBUG 0
/* turn this on for randomized tests. */
#define RANDKEY 0

/* encrypt / decrypt some strings, and make sure
 * this composition is the identity */

int main() {
	fprintf(stderr, "testing rsa...\n");
	char *pass,*fail;
	//fprintf(stdout, "1\n");
	if (isatty(fileno(stdout))) {
		//fprintf(stdout, "2\n");
		pass = "\033[32mpassed\033[0m";
		//fprintf(stdout, "3\n");
		fail = "\033[31mfailed\033[0m";
		//fprintf(stdout, "4\n");
	} else {
		pass = "passed";
		fail = "failed";
	}
	//fprintf(stdout, "7\n");

#if RANDKEY
	//fprintf(stdout, "5\n");
	setSeed(0,0);
	//fprintf(stdout, "6\n");
#else
	//printf("8\n");
	setSeed((unsigned char*)"random data :D:D:D",18);
	//printf("9\n");

#endif
	//printf("10\n");
	RSA_KEY K;
	//printf("11\n");
	rsa_keyGen(1024,&K);
	//printf("12\n");
	size_t i,j,ctLen,mLen = rsa_numBytesN(&K);
	//printf("13\n");
	unsigned char* pt = malloc(mLen);
	//printf("15\n");
	unsigned char* ct = malloc(mLen);
	//printf(stdo"16\n");
	unsigned char* dt = malloc(mLen);
	//printf("17\n");
	for (i = 0; i < 10; i++) {
		//printf("18\n");
		pt[mLen-1] = 0; /* avoid reduction mod n. */
		//printf("19\n");
		randBytes(pt,mLen-1);
		//printf("20\n");
		/* encrypt, decrypt, check. */
		
		//printf("Before = %u\n", *pt);
		ctLen = rsa_encrypt(ct,pt,mLen,&K);
		//printf("21\n");
		rsa_decrypt(dt,ct,ctLen,&K);
		//printf("22\n");
		//printf("After = %u\n\n", *dt);
		
		for (j = 0; j < mLen; j++) {
			if (dt[j] != pt[j]) break;
		}
		fprintf(stderr, "test[%02lu] %s\n",i,(j==mLen)?pass:fail);
	}
	free(pt); free(ct); free(dt);
	return 0;
}
