#!/bin/bash

if [[ ! -d data ]]; then
	mkdir data
	echo "Place test files in data/ before running this."
	exit 1
fi

# compile ske-test.c added by me
gcc ske-test.c ../rsa.c ../prf.c ../ske.c -lgmp -L/usr/local/lib/ -lcrypto -lssl -o ske-test


# reuse temp directory if present.
tdir=$(ls -d /tmp/ske-tdata-* 2> /dev/null)
[[ -z $tdir ]] && tdir=$(mktemp -d /tmp/ske-tdata-XXX)
match=0
total=0
for f in data/* ; do
	bn=${f##*/}
	# ./ske-test "$f" "$tdir/${bn}.enc" "$tdir/${bn}" # old one
	LD_LIBRARY_PATH=/usr/local/lib ./ske-test "$f" "$tdir/${bn}.enc" "$tdir/${bn}"
	if diff "$f" "$tdir/${bn}" ; then
		(( match++ ))
	fi
	(( total++ ))
	
	rm "$tdir/${bn}"
	rm "$tdir/${bn}.enc"
done
echo "$match out of $total files matched."

rm ske-test # also added by me
