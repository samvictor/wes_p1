#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "rsa.h"
#include "prf.h"

/* NOTE: a random composite surviving 10 Miller-Rabin tests is extremely
 * unlikely.  See Pomerance et al.:
 * http://www.ams.org/mcom/1993-61-203/S0025-5718-1993-1189518-9/
 * */
#define ISPRIME(x) mpz_probab_prime_p(x,10)
#define NEWZ(x) mpz_t x; mpz_init(x)
#define BYTES2Z(x,buf,len) mpz_import(x,len,-1,1,0,0,buf)
#define Z2BYTES(buf,len,x) mpz_export(buf,&len,-1,1,0,0,x)

/* utility function for read/write mpz_t with streams: */
int zToFile(FILE* f, mpz_t x)
{
	size_t i,len = mpz_size(x)*sizeof(mp_limb_t);
	unsigned char* buf = malloc(len);
	/* force little endian-ness: */
	for (i = 0; i < 8; i++) {
		unsigned char b = (len >> 8*i) % 256;
		fwrite(&b,1,1,f);
	}
	Z2BYTES(buf,len,x);
	fwrite(buf,1,len,f);
	/* kill copy in buffer, in case this was sensitive: */
	memset(buf,0,len);
	free(buf);
	return 0;
}
int zFromFile(FILE* f, mpz_t x)
{
	size_t i,len=0;
	/* force little endian-ness: */
	for (i = 0; i < 8; i++) {
		unsigned char b;
		/* XXX error check this; return meaningful value. */
		fread(&b,1,1,f);
		len += (b << 8*i);
	}
	unsigned char* buf = malloc(len);
	fread(buf,1,len,f);
	BYTES2Z(x,buf,len);
	/* kill copy in buffer, in case this was sensitive: */
	memset(buf,0,len);
	free(buf);
	return 0;
}

int rsa_keyGen(size_t keyBits, RSA_KEY* K)
{
	rsa_initKey(K);
	/* TODO: write this.  Use the prf to get random byte strings of
	 * the right length, and then test for primality (see the ISPRIME
	 * macro above).  Once you've found the primes, set up the other
	 * pieces of the key ({en,de}crypting exponents, and n=pq). */

	unsigned char* rand_num = malloc(keyBits);
	
	int temp_prime = 0;
	while (temp_prime == 0)
	{
		randBytes(rand_num, keyBits/8);
		BYTES2Z(K->p, rand_num, keyBits);
		temp_prime = ISPRIME(K->p);
	}
	mpz_t phi_p;
	mpz_init(phi_p);
	mpz_sub_ui(phi_p, K->p, 1);
	
	//printf("For p:\nIs Prime = %d\n", ISPRIME(K->p));
	//printf("p = %#08lx\n", mpz_get_ui(K->p));
	//printf("phi(p) = %#08lx\n\n", mpz_get_ui(phi_p));
	
	temp_prime = 0;
	while (temp_prime == 0)
	{
		randBytes(rand_num, keyBits/8);
		BYTES2Z(K->q, rand_num, keyBits);
		temp_prime = ISPRIME(K->q);
	}
	mpz_t phi_q;
	mpz_init(phi_q);
	mpz_sub_ui(phi_q, K->q, 1);
	
	//printf("For q:\nIs Prime = %d\n", ISPRIME(K->q));
	//printf("q = %#08lx\n", mpz_get_ui(K->q));
	//printf("phi(q) = %#08lx\n\n", mpz_get_ui(phi_q));
	
	mpz_mul(K->n, K->p, K->q); 
	mpz_t phi_n;
	mpz_init(phi_n);
	mpz_mul(phi_n, phi_p, phi_q);
	//printf("For n:\nn = %#08lx\n", mpz_get_ui(K->n));
	//printf("phi(n) = %#08lx\n\n", mpz_get_ui(phi_n));
	
	mpz_t temp_mpz;
	mpz_init(temp_mpz);
	mpz_set_ui(temp_mpz, 2);
	int inv_found = 0;
	
	while(mpz_get_ui(temp_mpz) != 1 || inv_found == 0)
	{
		randBytes(rand_num, keyBits/8);
		BYTES2Z(K->e, rand_num, keyBits);
		mpz_gcd(temp_mpz, K->e, phi_n);
		//printf("GCD = %#08lx\n\n", mpz_get_ui(temp_mpz));
		
		if (mpz_invert(K->d, K->e, phi_n) != 0)
			inv_found = 1;
		
		//printf("Inverse = %#08lx\n\n", mpz_get_ui(K->d));
		//mpz_mul(temp_mpz, K->d, K->e);
		//mpz_mod(temp_mpz, temp_mpz, phi_n);
		//printf("E*D = %#08lx\n\n", mpz_get_ui(temp_mpz));
	}
	
	//gmp_printf ("%s is K->e %Zd\n", "here", K->e);
	//gmp_printf ("%s is K->d %Zd\n", "here", K->d);
	
	//printf("\n\n");
	return 0;
}

size_t rsa_encrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		RSA_KEY* K)
{
	/* TODO: write this.  Use BYTES2Z to get integers, and then
	 * Z2BYTES to write the output buffer. */
	//int i;
	//for (i = 0; i < len; i++)
	//	printf("%u ", *(inBuf+i));
	//printf("\n\n");
	
	mpz_t data;
	mpz_init(data);
	BYTES2Z(data,inBuf,len);
	//printf("Before = %lu\n", mpz_get_ui (data));
	
	// data = data^e mod n
	mpz_powm(data, data, K->e, K->n);	
	//printf("%lu", mpz_get_ui (data));	
	
	Z2BYTES(outBuf, len, data);
	
	//int j;
	//for (j = 0; j < len; j++)
	//	printf("%u ", *(outBuf+j));
	//printf("\n\n");
	
	return len; /* TODO: return should be # bytes written */
}
size_t rsa_decrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		RSA_KEY* K)
{
	/* TODO: write this.  See remarks above. */
	//int i;
	//for (i = 0; i < len; i++)
	//	printf("%u ", *(inBuf+i));
	//printf("\n\n");
	
	mpz_t data;
	mpz_init(data);
	BYTES2Z(data,inBuf,len);
	//printf("%lu", mpz_get_ui (data));
	
	// data = data^e mod n
	mpz_powm(data, data, K->d, K->n);	
	//printf("After = %lu\n\n", mpz_get_ui (data));	
	
	Z2BYTES(outBuf, len, data);
	
	//int j;
	//for (j = 0; j < len; j++)
	//	printf("%u ", *(outBuf+j));
	//printf("\n\n");
	
	
	return len;
}

size_t rsa_numBytesN(RSA_KEY* K)
{
	return mpz_size(K->n) * sizeof(mp_limb_t);
}

int rsa_initKey(RSA_KEY* K)
{
	mpz_init(K->d); mpz_set_ui(K->d,0);
	mpz_init(K->e); mpz_set_ui(K->e,0);
	mpz_init(K->p); mpz_set_ui(K->p,0);
	mpz_init(K->q); mpz_set_ui(K->q,0);
	mpz_init(K->n); mpz_set_ui(K->n,0);
	return 0;
}

int rsa_writePublic(FILE* f, RSA_KEY* K)
{
	/* only write n,e */
	zToFile(f,K->n);
	zToFile(f,K->e);
	return 0;
}
int rsa_writePrivate(FILE* f, RSA_KEY* K)
{
	zToFile(f,K->n);
	zToFile(f,K->e);
	zToFile(f,K->p);
	zToFile(f,K->q);
	zToFile(f,K->d);
	return 0;
}
int rsa_readPublic(FILE* f, RSA_KEY* K)
{
	rsa_initKey(K); /* will set all unused members to 0 */
	zFromFile(f,K->n);
	zFromFile(f,K->e);
	return 0;
}
int rsa_readPrivate(FILE* f, RSA_KEY* K)
{
	rsa_initKey(K);
	zFromFile(f,K->n);
	zFromFile(f,K->e);
	zFromFile(f,K->p);
	zFromFile(f,K->q);
	zFromFile(f,K->d);
	return 0;
}
int rsa_shredKey(RSA_KEY* K)
{
	/* clear memory for key. */
	mpz_t* L[5] = {&K->d,&K->e,&K->n,&K->p,&K->q};
	size_t i;
	for (i = 0; i < 5; i++) {
		size_t nLimbs = mpz_size(*L[i]);
		if (nLimbs) {
			memset(mpz_limbs_write(*L[i],nLimbs),0,nLimbs*sizeof(mp_limb_t));
			mpz_clear(*L[i]);
		}
	}
	/* NOTE: a quick look at the gmp source reveals that the return of
	 * mpz_limbs_write is only different than the existing limbs when
	 * the number requested is larger than the allocation (which is
	 * of course larger than mpz_size(X)) */
	return 0;
}
